

(function () {
    pagination(true);
})();

$(function() {
    $('.ui.dropdown').dropdown();
});

var mySiteConfig = {
    locales: [
        { code: "nl", name: "Nederlands" },
        { code: "fr", name: "Français" },
        { code: "en", name: "English" }
    ],
    // Add other site-wide configurations here
};

window.mySiteConfig = mySiteConfig;


// (function () {
//     const mediaQuery = window.matchMedia('(max-width: 767px)');
//
//     // IMPORTANT: For themes other than Casper, change the selector just below to select your theme's header menu selector
//     const menu = document.querySelector('.gh-head-menu');
//     const nav = menu.querySelector('.nav');
//     if (!nav) return;
//
//     // IMPORTANT: For themes other than Casper, change the selector just below to select your theme's header logo selector
//     const logo = document.querySelector('.gh-head-logo');
//     const navHTML = nav.innerHTML;
//
//     if (mediaQuery.matches) {
//         const items = nav.querySelectorAll('li');
//         items.forEach(function (item, index) {
//             item.style.transitionDelay = 0.03 * (index + 1) + 's';
//         });
//     }
//
//     const makeHoverdown = function () {
//         if (mediaQuery.matches) return;
//
//         var hoverDown_list = [],
//             latest_navigation_item,
//             // IMPORTANT: For themes other than Casper, change the selector just below to select your theme's header menu item selector
//             nav_list = document.querySelectorAll('.gh-head-menu li');
//         var newMenuList = [];
//         var menuTree = {};
//
//         nav_list.forEach( (item, index) => {
//             if (item.childNodes[0].innerText.startsWith('-')) {
//                 if(menuTree[newMenuList.length - 1]) {
//                     menuTree[newMenuList.length - 1].push(item);
//                 } else {
//                     menuTree[newMenuList.length - 1] = [item];
//                 }
//             } else {
//                 newMenuList.push(item);
//             }
//         });
//
//         nav_list = newMenuList.map((item, index) => {
//             if (menuTree[index]) {
//                 let hoverdown = document.createElement('ul');
//                 hoverdown.className = 'isHoverDown';
//                 menuTree[index].forEach(child => {
//                     hoverDown_item_text = child.childNodes[0].innerText;
//                     child.childNodes[0].innerText = hoverDown_item_text.replace('- ', '');
//                     hoverdown.appendChild(child);
//                 });
//                 item.className += '--hasHoverDown';
//                 item.appendChild(hoverdown);
//             }
//             return item;
//         });
//     }
//
//     imagesLoaded(logo, function () {
//         makeHoverdown();
//     });
//
//     window.addEventListener('resize', function () {
//         setTimeout(function () {
//             nav.innerHTML = navHTML;
//             makeHoverdown();
//         }, 1);
//     });
//
// })();
